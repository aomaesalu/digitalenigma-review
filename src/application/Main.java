package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import data.Rotors;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import mechanics.Enigma;
import mechanics.Plugboard;
import mechanics.Rotor;

// "Programmeerimise suvekursuse" �levaataja: Ants-Oskar M�esalu

/*
 * TODO: Graafilise kasutajaliidese poolest tuleks ilmselt veel vaeva n�ha
 * sellega, et iga asi ei avaneks uues aknas. Kasutatavuse poolest v�ib see
 * aspekt �sna t��tuks osutuda.
 * TODO: Programmikoodis tuleks muutujate nimetamisel �ra otsustada, kas 
 * kasutada eesti v�i inglise keelt. Segakeeles kirjutatud koodi on suhteliselt
 * segane lugeda.
 */

/**
 * Digitaalse Enigma p�hiprogramm, mis realiseerib II maailmas�jas kasutatud
 * Enigma-nimelise s�numite �ifreerimiseks m�eldud portatiivse masina Javas
 * koos graafilise kasutajaliidesega JavaFX baasil.
 * RW: Lisasin dokumentatsiooniks kommentaari.
 * @author Ott Matiisen
 */
public class Main extends Application {

	/**
	 * Meetod, mis loob graafilisele kasutajaliidesele �lemise men��.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * @param menuWidthProperty
	 * @return
	 */
	private MenuBar createMenuBar(ReadOnlyDoubleProperty menuWidthProperty) {
		MenuBar menuBar = new MenuBar();
		Menu menuFile = new Menu("File");
		Menu menuAbout = new Menu("About");

		MenuItem newEnigma = new MenuItem("New Enigma");
		newEnigma.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				createNewEnigma();
			}
		});
		MenuItem options = new MenuItem("Options");
		options.setDisable(true);
		options.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				openEditWindow();
			}
		});
		MenuItem exit = new MenuItem("Exit");
		exit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				System.exit(0);
			}
		});
		menuFile.getItems().addAll(newEnigma, options, new SeparatorMenuItem(), exit);

		MenuItem info = new MenuItem("Info");
		info.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				openInfoWindow();
			}
		});
		menuAbout.getItems().add(info);

		menuBar.getMenus().addAll(menuFile, menuAbout);
		menuBar.prefWidthProperty().bind(menuWidthProperty);

		return menuBar;
	}

	/**
	 * Meetod, mis avab uue akna koos programmi looja kontaktandmetega.
	 * Kutsutakse v�lja �lemen��st "Info" nupule vajutamisel.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 */
	private void openInfoWindow() {
		Stage stage = new Stage();
		GridPane root = new GridPane();
		root.setPadding(new Insets(10, 15, 15, 10));
		setStageSize(stage, 100, 180);
		Label labelNimi = new Label("Ott Matiisen");
		Label labelKontakt = new Label("ottmatiisen@hotmail.com");
		root.add(labelNimi, 0, 6);
		root.add(labelKontakt, 0, 7);

		Scene scene = new Scene(root, 40, 100);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Meetod, mis m��rab parameetrina saadud stage'ile staatilised m��tmed.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * @param stage
	 * @param height
	 * @param width
	 */
	private void setStageSize(Stage stage, double height, double width) {
		stage.setMaxHeight(height);
		stage.setMaxWidth(width);
		stage.setMinHeight(height);
		stage.setMinWidth(width);
	}

	/**
	 * Meetod, mis avab programmi s�tete muutmise akna. Kutsutakse v�lja
	 * �lamen��st nupule "Options" vajutamisel. Kuna programmikood pole veel
	 * t�ielikult valmis ja hetkel on vastav nupp vaikimisi keelatud, siis
	 * praktikas seda meetodit ei kasutata.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * RW: M�tlesin meetodi eemaldamisele, kuna seda reaalselt ei kasutata,
	 * aga j�tsin selle alles, kuna men��nupp "Options" s�ltuks sellest.
	 */
	private void openEditWindow() {
		Stage stage = new Stage();
		Pane root = new Pane();
		Scene scene = new Scene(root, 40, 100);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Meetod, mis loob uue virtuaalse Enigma masina ja kuvab selle 
	 * graafilises kasutajaliideses uues aknas. Programm v�imaldab 
	 * paralleelselt t��tada mitmete erinevate Enigma masinatega korraga.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 */
	private void createNewEnigma() {
		Enigma enigma = new Enigma(new Rotor(Rotors.ROTOR1.getCipher()), // RW: Viisin konstantsed literaalid �le enumeraatorisse
				new Rotor(Rotors.ROTOR2.getCipher()),
				new Rotor(Rotors.ROTOR3.getCipher()),
				new Rotor(Rotors.MIRROR.getCipher()),
				new Plugboard()); // RW: Eemaldasin Plugboardi parameetri
		Stage stage = new Stage();
		Group root = new Group();
		Scene scene = new Scene(root);
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 15, 10, 15));
		root.getChildren().add(grid);

		grid.getColumnConstraints().add(new ColumnConstraints());
		grid.getRowConstraints().add(new RowConstraints());
		grid.add(new Label("S�num:"), 0, 0);
		grid.add(new Label("�ifreerituna:"), 0, 3);
		TextField message = new TextField("Kirjuta siia");
		grid.add(message, 3, 0);
		grid.getRowConstraints().add(new RowConstraints(20));
		grid.getColumnConstraints().add(new ColumnConstraints(20));
		Label messageLabel = new Label();
		grid.add(messageLabel, 3, 3);
		Button cipher = new Button("Cipher");
		cipher.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				String cipheredMessage = enigma.cipher(message.getText());
				messageLabel.setText(cipheredMessage);
			}
		});
		grid.add(cipher, 5, 0);
		Button backButton = new Button("Back");
		backButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
		grid.add(backButton, 5, 11);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * Meetod, mis kirjutab aknasse stardimen�� "Start" ja "Exit" nuppudega.
	 * Kasutatakse kohe programmi k�ivitamisel esimeses akna loomisel.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * @param root
	 * @return
	 * @throws FileNotFoundException
	 */
	private Scene createStartMenu(Group root) throws FileNotFoundException {
		// RW: Pildi lisamine ekraanile oli v�lja kommenteeritud. Panin selle tagasi, kuna programm n�eb niimoodi etem v�lja.
		Scene scene = new Scene(root);
		ImageView splashScreen = new ImageView(new Image(new
		FileInputStream("img//Enigma.jpg")));
		splashScreen.setOpacity(0.5); // RW: Lisasin, et nupud v�lja paistaksid
		root.getChildren().addAll(splashScreen);
		GridPane pane = new GridPane();
		pane.setPadding(new Insets(100, 40, 40, 90));
		Button start = new Button("Start");
		start.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				createNewEnigma();
			}
		});
		pane.getColumnConstraints().add(new ColumnConstraints(3));
		pane.add(start, 0, 0, 2, 1);
		Button exit = new Button("Exit");
		exit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				System.exit(0);
			}
		});
		pane.getRowConstraints().add(new RowConstraints(60));
		pane.add(exit, 1, 2);
		root.getChildren().add(pane);
		return scene;
	}

	/**
	 * Meetod, mis loob graafilise kasutajaliigese esmase programmiakna
	 * ja asetab sinna k�ik programmi kujunduslikud elemendid.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			setStageSize(primaryStage, 350, 250);
			Group rootGroup = new Group();
			MenuBar menuBar = createMenuBar(primaryStage.widthProperty());
			Scene scene = createStartMenu(rootGroup);
			rootGroup.getChildren().addAll(menuBar);
			// RW: Removed empty CSS file
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("Digital Enigma");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * P�himeetod, mis v�ljastatakse kohe programmi k�ivitamisel. M��rab, et
	 * graafilise kasutajaliidesega rakendus k�ivitatakse vahetult.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}