package data;

/**
 * Enigma �ifreerimismasina rootoritel eraldi m��ratud t�hestike enumeraator.
 * @author Ants-Oskar M�esalu
 */
public enum Rotors {
	ROTOR1("JGDQOXUSCAMIFRVTPNEWKBLZYH"),
	ROTOR2("NTZPSFBOKMWRCJDIVLAEYUXHGQ"),
	ROTOR3("JVIUBHTCDYAKEQZPOSGXNRMWFL"),
	MIRROR("QYHOGNECVPUZTFDJAXWMKISRBL");
	
	private String cipher;
	
	/**
	 * Privaatne konstruktor.
	 * @param cipher
	 */
	private Rotors(String cipher) {
		this.cipher = cipher;
	}
	
	/**
	 * Tagasta rootorile s�testatud �iffer.
	 * @return
	 */
	public String getCipher() {
		return cipher;
	}
}
