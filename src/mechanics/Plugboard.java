package mechanics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//"Programmeerimise suvekursuse" �levaataja: Ants-Oskar M�esalu

/*
 *  TODO: RW: Ideaalis peaks tulevikus olema v�imalik plugboardi s�tteid ka salvestada, et
 *  programm oleks kasutatav ka s�numite de�ifreerimiseks. �ifreeritud s�numitest, mida
 *  ei saa lahti teha, pole ilmselt suuremat kasu kui huviline eesm�rk. 
 */

/** 
 * Plugboard on klass, mis realiseerib Enigmas kasutatud plugboardi', 
 * milles asetati t�hestiku t�hed paarikaupa omavahelistesse s�ltuvustesse.
 * Seej�rel, enne rootoritesse signaalide saatmist, vahetati klaviatuuril
 * esinenud t�ht selle vastega plugboard'i t�hepaaride j�rgi �ra.
 * RW: Lisasin dokumentatsiooniks kommentaari.
 * @author Ott Matiisen
 */
public class Plugboard {
	private Map<Character, Character> pairs = new HashMap<Character, Character>(); // RW: Muutsin privaatseks, et Plugboardiga klassiv�liselt manipuleerida ei saaks

	/**
	 * Konstruktor, milles realiseeritakse plugboard'i idee, kusjuures
	 * parasjagused s�tted genereeritakse suvaliselt.
	 * Standardi kohaselt kasutati Enigma masinas plugboard'il 10 juhtmepaari,
	 * �lej��nud 6 t�hte �hendati iseendasse.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 */
	public Plugboard() { // RW: Eemaldasin kasutamata parameetri
		super();
		// RW: Eemaldasin alati t�ese valikulause
		ArrayList<Integer> kasutatud = new ArrayList<Integer>(); // RW: �ldjuhul kasutatakse konstruktsiooni List<?> myList = new ArrayList<?>(), kuna see on palju paindlikum, ent antud juhul pole t�epoolest lisafunktsionaalsust tarvis ning ArrayList ajab asja �ra - v�hem koodi importimist!
		int i = 0;
		while (i < 20) {
			int katse = (int) (Math.round((Math.random() * 25) + 65));
			if (!kasutatud.contains(katse)) {
				kasutatud.add(katse);
				i++;
				while (i % 2 != 0) { // RW: Parandasin paarsuskontrolli hoiatuse
					int vaste = (int) (Math.round((Math.random() * 25) + 65));
					if (!kasutatud.contains(vaste)) {
						kasutatud.add(vaste);
						i++;
						addPair((char) katse, (char) vaste); // RW: Eemaldasin ebavajaliku this-viite
					}
				}
			}
		}
		addOthers(); // RW: Eemaldasin ebavajaliku this-viite
	}

	/**
	 * Lisa plugboard'ile t�hepaar. T�hepaari s�ltuvus realiseeritakse
	 * m�lemapidiselt. Kui t�hepaare on juba 26 v�i rohkem olemas, siis 
	 * uut t�hepaari ei lisata ning visatakse vastava veateatega erind.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * @param one
	 * @param two
	 */
	private void addPair(char one, char two) { // RW: Muutsin privaatseks meetodiks, et Plugboardiga v�liselt manipuleerida ei saaks
		if (pairs.size() < 26) {
			pairs.put(one, two);
			pairs.put(two, one);
		} else {
			throw new RuntimeException("Viga: Plugboard'il on liiga palju \"juhtmepaare\".");
		}
	}

	/**
	 * Lisa plugboard'ile puuduolevad t�hepaarid ning realiseeri need
	 * �ks-�hese seosena (�hendatud iseendasse).
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 */
	private void addOthers() { // RW: Muutsin privaatseks, et Plugboardiga v�liselt manipuleerida ei saaks
		for (char i = 'A'; i <= 'Z'; i++) {
			pairs.putIfAbsent(i, i);
		}
	}

	/**
	 * Tagasta t�hepaari teine paariline esimese j�rgi vastavalt plugboard'i
	 * reeglistikule.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * @param ch
	 * @return
	 */
	public char translate(char ch) {
		if (pairs.get(ch) != null) { // RW: Kontrollin, et sisend oleks korrektne
			return pairs.get(ch);
		} else {
			throw new RuntimeException("Viga: Plugboard'il ei asu sisestatud t�hte \"" + ch + "\".");
		}
	}
}
