package mechanics;

//"Programmeerimise suvekursuse" �levaataja: Ants-Oskar M�esalu

/**
 * Rotor on klass, mis realiseerib Enigma masina rootori t��.
 * Enigma masina k�ige olulisem osa koosnes 3-st vahetatavast rootorist, mis
 * defineerisid mingid kindlad substitutsioonid t�htedele. Iga kord, kui m�ni
 * t�ht �ifreeritavast tekstist l�biti, p��rati rootoreid edasi. Kui rootor
 * j�udis oma algpositsiooni, p��rati j�rgmist rootorit edasi. See t�hendas
 * t�hestikule 26^3 = 17576 erinevat positsiooni, millele omakorda lisanduvad
 * igale rootorile vastavast �ifrist tulenevad lisav�imalused (traditsiooniliselt
 * neid v�imalusi siiski t�en�oliselt ei kasutatud, vaid selleks oli olemas 
 * plugboard).
 * RW: Lisasin dokumentatsiooni kommentaari.
 * @author Ott Matiisen
 */
public class Rotor {
	private String cipher;
	private int position; // RW: Eemaldasin algv��rtustamise - see oleks ilus teostada konstruktoris.
	private int initialPosition; // RW: Lisasin algpositsiooni v��rtuse, et rootori algs�testamisel mingile muule positsioonile kasutataks seda j�rgmise rootori p��ramise tingimuste t�itmiseks.

	/**
	 * Konstruktor rootori loomiseks ainult �ifri parameetriga. Rootor asetatakse
	 * algpositsiooni.
	 * RW: T�iendasin dokumentatsiooni kommentaari.
	 * @param cipher
	 */
	public Rotor(String cipher) {
		super();
		this.cipher = cipher;
		this.position = 0;
		this.initialPosition = 0;
	}
	
	/**
	 * Konstruktor rootori loomiseks nii �ifri kui ka rootori algpositsiooni 
	 * parameetriga. Rootor luuakse vastavalt parameetris asuvale �ifrile ning 
	 * asetatakse kirjeldatud positsiooni. Kui positsioonile vastav number on
	 * ebareaalne, visatakse vastava veateatega erind.
	 * RW: Lisasin konstruktori.
	 * @param cipher
	 * @param initialPosition
	 */
	public Rotor(String cipher, int initialPosition) {
		super();
		this.cipher = cipher;
		if (initialPosition >= 0 && initialPosition < cipher.length()) {
			this.position = initialPosition;
			this.initialPosition = initialPosition;
		} else {
			throw new RuntimeException("Viga! Sisendist saadud rootori algasend ei ole v�imalik.");
		}
	}
	
	/**
	 * Tagastab rootori asendi.
	 * @return
	 */
	public int getPosition() {
		return position;
	}

	/*
	 * RW: Eemaldasin meetodi setPosition(int), et rootoreid ei saaks suvaliselt 
	 * �mber asetada. Kui soovida realiseerida rootorite �igesse asendisse
	 * paigutamine, et s�numeid ka de�ifreerida (mis ideaalis oleks j�rgmine 
	 * eesm�rk), siis tuleks selleks luua rootoritele vastavad konstruktorid -
	 * rootorite endi t�� k�igus nende asendeid suvaliselt muuta ei tohiks.
	 */
	
	/**
	 * P��rab rootorit �he v�rra edasi. Kui rootori positsioon j�uab maksimaalse
	 * v��rtuseni, muutub see taas 0-ks, et rootor t�epoolest ka rootorina 
	 * toimiks. Rootori suurus (ja seega v�imalike positsioonide arv) s�ltub 
	 * temasse asetatud �ifri suurusest.
	 * RW: L�in uue meetodi.
	 */
	public void turn() {
		position = (position + 1) % cipher.length();
	}
	
	/**
	 * Tagastab, kas rootor on oma algasendis. Seda meetodit kasutatakse, kui
	 * soovitakse kontrollida, kas p��rata tuleks ka j�rgmist rootorit.
	 * RW: Lisasin meetodi.
	 * @return
	 */
	public boolean isInInitialPosition() {
		return position == initialPosition;
	}
	
	/**
	 * V�tab t�hestiku t�he ning tagastab �ifrist vastavast asendist t�he.
	 * Kuna java jaoks on "char" ehk t�ht �htlasi t�isarv, saab ASCII t�hest
	 * 65 lahutades t�he indeksi t�hestikus ("A" on ASCII's 65 ning t�hestikus
	 * indeksiga 0). Kuna minu masinas liigub rootori �iffer paremale, 
	 * [...]
	 * RW: Kommentaar on l�petamata j��nud - mida siin m�eldi?
	 * @param letter
	 * @return
	 */
	public char into(char letter) {
		if (letter >= 65 && letter <= 90) { // RW: Kontrollin, et sisend oleks korrektne
			if ((letter - 65 - position) >= 0) {
				return cipher.charAt(letter - 65 - position);
			} else {
				return cipher.charAt(letter - 39 - position);
			}
		} else {
			throw new RuntimeException("Sisendist leiti t�ht, mis ei asu 'A' ja 'Z' vahel.");
		}
	}

	/**
	 * V�tab �ifrist t�he ning tagastab t�he vaste t�hestikus.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * @param letter
	 * @return
	 */
	public char out(char letter) {
		if (letter >= 65 && letter <= 90) { // RW: Kontrollin, et sisend oleks korrektne
			if ((cipher.indexOf(letter) + position + 65) < 91) {
				return (char) (cipher.indexOf(letter) + position + 65);
			} else {
				return (char) (cipher.indexOf(letter) + position + 39);
			}
		} else {
			throw new RuntimeException("Sisendist leiti t�ht, mis ei asu 'A' ja 'Z' vahel.");
		}
	}
}