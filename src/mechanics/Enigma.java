package mechanics;

//"Programmeerimise suvekursuse" �levaataja: Ants-Oskar M�esalu

/**
 * TODO: RW: Tulevikus oleks ilmselt ideaalplaanis hea lisada ka 
 * de�ifreerimisvahendid algsete s�numite taastamiseks. Selleks
 * tuleks aga luua vahendid rootorite vastavate algpositsioonide ja
 * plugboardi s�tete salvestamiseks, et neid hiljem juba �ifreeritud
 * s�numite de�ifreerimisel kasutada saaks - kasutama peab samu s�tteid,
 * see oligi Enigma masina tugevus.
 */

/**
 * Enigma on klass, mis realiseerib kogu Enigma kr�pteerimismasina t��.
 * RW: Lisasin dokumentatsiooniks kommentaari.
 * @author Ott Matiisen
 */
public class Enigma {
	private Rotor rotor1;
	private Rotor rotor2;
	private Rotor rotor3;
	private Rotor mirror;
	private Plugboard plugboard;

	/**
	 * Konstruktor.
	 * @param rotor1
	 * @param rotor2
	 * @param rotor3
	 * @param mirror
	 * @param plugboard
	 */
	public Enigma(Rotor rotor1, Rotor rotor2, Rotor rotor3, Rotor mirror,
			Plugboard plugboard) {
		super();
		this.rotor1 = rotor1;
		this.rotor2 = rotor2;
		this.rotor3 = rotor3;
		this.mirror = mirror;
		this.plugboard = plugboard;
	}

	/*
	 * RW: Eemaldasin meetodi getCalibration(), mis pidi tagastama t�isarvuj�rjendi
	 * rootorite parasjagustest positsioonidest.
	 */

	/**
	 * P��ra rootorite s�steemi. Kui m�ni esimestest rootoritest j�uab oma algolekusse,
	 * p��ratakse temale j�rgnevat rootorit, vastasel juhul mitte, jne.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 */
	public void turn() {
		// RW: Kirjutasin meetodi �mber nii, et rootorite p��ramine oleks turvalisemalt realiseeritud (et neid ei saaks lihtsalt suvalistesse asenditesse asetada).
		rotor3.turn();
		if (rotor3.isInInitialPosition()) {
			rotor2.turn();
			if (rotor2.isInInitialPosition()) {
				rotor1.turn();
			}
		}
	}

	/*
	 * RW: Eemaldasin meetodi calibrate(int, int, int), kuna seda ei kasutatud 
	 * programmikoodis mitte kuskil. Lisaks ei oleks loogiline rootorite positsiooni
	 * nende masinasolemise ajal k�sitsi muuta - sel ajal rakendatakse ainult 
	 * p��ramisoperatsioone. Rootorite algs�tete seadmiseks tuleks kasutada
	 * rootorite kahe parameetriga konstruktoreid, mis lisaks �ifrile m��ravad
	 * ka rootorite algpositsioonid.
	 */
	
	/**
	 * Tagasta parameetrist saadud tekstiline s�num Enigma-masinale sobilikule
	 * tekstilisele kujule vormindatuna.
	 * RW: T�stsin koodi cipher(String) meetodist siia �mber, et koodis selgust luua.
	 * @param message
	 * @return
	 */
	private String formatMessage(String message) {
		message = message.toUpperCase();
		message = message.replaceAll("�", "AE");
		message = message.replaceAll("�", "Y");
		message = message.replaceAll("�", "OE");
		message = message.replaceAll("�", "O");
		for (int i = 0; i < message.length(); i++) {
			if ((message.charAt(i) < 'A') || (message.charAt(i) > 'Z')) {
				message = message.replaceAll(Character.toString(message.charAt(i)), " ");
			}
		}
		String[] strippedText = message.split("");
		String properMessage = "";
		for (String bit : strippedText) {
			properMessage += bit.trim();
		}
		return properMessage;
	}

	/**
	 * Tagasta s�num �ifreeritud kujul.
	 * RW: Lisasin dokumentatsiooniks kommentaari.
	 * @param message
	 * @return
	 */
	public String cipher(String message) {
		message = formatMessage(message); // RW: Liigutasin parameetrist saadud s�numi reeglitele vastavaks vormindamise eraldi meetodisse, et meetod liiga pikaks ja segaseks ei muutuks (t�itis korraga mitut t���lesannet).
		String cipheredMessage = "";
		for (int i = 0; i < message.length(); i++) {
			if (message.charAt(i) > 64 && message.charAt(i) < 91) {
				this.turn();
				char letter = plugboard.translate(rotor1.out(rotor2.out(rotor3.out(
						mirror.into(rotor3.into(rotor2.into(rotor1.into(
								plugboard.translate(message.charAt(i))))))))));
				cipheredMessage += letter;
			} else {
				throw new RuntimeException("S�mbol ei kuulu ladina t�hestikku: "
								+ message.charAt(i));
			}
		}
		return cipheredMessage;
	}
}